> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Eliezer Penias

The purpose of this assignment is to practice client-side validation.

### Project 1 Requirements:


1. Provide **Bitbucket** read-only access to **p1** repo, *must* include **README.md**, using Markdown
  syntax.
2. Blackboard Links: p1 Bitbucket repo

#### Assignment Screenshots:

**Failed Validation:**
![Failed Validation](img/failedv.png)

**Passed Validation:**
![Passed Validation](img/passedv.png)